> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331

## Derek Piccininni

### Assignment #3 Requirements:

*Sub-Heading:*

1. Field to enter U.S. dollar amount: 1–100,000
2. Must include toast notification if user enters out-of-range values
3. Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency
4. Must include correct sign for euros, pesos, and Canadian dollars
5. Must Add background colors or theme
6. Create and display launcher icon image
7. Create splash/Loading Screen

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1 
* Screenshot of running application’s splash 
* Screenshot of running application’s unpopulated user interface
* Screenshot of running application’s toast notification
* Screenshot of running application’s converted currency user interface

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Splash Screen Screenshot*:

![Splash Screen](CCsplash.png)


*Screenshot of Unpopulated Currency App*:

![Unpopulated](UnpopCC.png)


*Screenshot of Populated Currency Appf*:

![Running](RunningCC.png)

*Screenshot of Toast Notification*:

![Toast](ToastNoti.png)

*Screenshot SS4*:

![Toast](SS3.png)

#### Assignment Screenshots SS5:

*Even or Odd Calculator Message*:

![Toast](GUImessage.png)

*Even or Odd Calculator Prompt*:

![Toast](GUIprompt.png)

*Even or Odd Calculator Odd number*:

![Toast](GUIodd.png)

*Even or Odd Calculator Even number*:

![Toast](GUIeven.png)

*Even or Odd Calculator Invalid Input*:

![Toast](Invalid.png)

#### Assignment Screenshots SS6:

*Paint Calculator Prompt*:

![Toast](PCCprompt.png)

*Paint Calculator Heightt*:

![Toast](PCCheight.png)

*Paint Calculator Widtht*:

![Toast](PCCwidth.png)

*Paint Calculator Length*:

![Toast](PCClength.png)

*Paint Calculator Price*:

![Toast](PCCprice.png)

*Paint Calculator Final*:

![Toast](PCCfinal.png)









