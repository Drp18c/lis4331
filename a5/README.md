> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331

## Derek Piccininni

### Assignment #5 Requirements:

*Sub-Heading:*

1. Main screen with app title and articles
2. RSS feed
3. Background colors or theme
4. Launcher Icon

#### README.md file should include the following items:

* Screenshot of running main screen
* Screenshot of individual article	
* Screenshot of default browser


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

*Screenshot of Main Screen*:

![AMPPS Installation Screenshot](NewReader.png)

*Screenshot of individual article*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of default browser*:

![Android Studio Installation Screenshot](img/android.png)

*Screenshot of SS13*:

![Android Studio Installation Screenshot](redoss.png)

*Screenshot of SS14*:

![Android Studio Installation Screenshot](4331ss14.png)

*Screenshot of SS15*:

![Android Studio Installation Screenshot](4331ss15.png)

