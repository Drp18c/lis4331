

# LIS 4331

## Derek Piccininni

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and BitBucket
2. Development Operations
3. Chapter Questions (Ch.1,2)
4.BitBucket repo links a.) This assignment and b.)the completed tutorials above (bitbuckqetstationslocations and myteamquotes).

#### README.md file should include the following items:

* Screenshot of running Java Hello
* Screenshot of running Android Studio- my First App
* Screenshots of running Android Studio- Contacts App
* Git commands with short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- Creates new local repository
2. git status- Lists the files you've changed
3. git add- Add one or more files
4. git commit- Commit changes
5. git push- Sends changes to remote repository 
6. git pull- Gets changes from your remote directory to your working directory
7. git checkout- To undo local changes
 

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![image](hellojava.png)


*Screenshot of Android Studio - My First App*:

![image](firstapp.png)

*Screenshot of Android Studio - Contacts App*:

![image](contacthome.png)
![image](front.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Drp18c/bitbucketstationlocations/src/master/)


