> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331

## Derek Piccininni

### Assignment P#1 Requirements:

*Sub-Heading:*

1. Create music app that includes spas screen, image, app title, and intro text 
2. Includes artists' images and media
3. Images and buttons must be vertically and horizontally aligned
4. Must add background colors or theme
5. Create and display icon image

#### README.md file should include the following items:

* Screenshot of Splash Screen
* Screenshot of follow-up Screen
* Screenshot of play and pause user interface


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of running splash screen*:

![AMPPS Installation Screenshot](p1Splash.png)

*Screenshot of running app *:

![JDK Installation Screenshot](p1notplaying.png)

*Screenshot of running app playing*:

![Android Studio Installation Screenshot](p1playing.png)

*Screenshot of paused app*:

![Android Studio Installation Screenshot](p1playing.png)

*Screenshot of SS7*:

![Android Studio Installation Screenshot](SS7.png)

*Screenshot of SS8 Empty*:

![Android Studio Installation Screenshot](SS8empty.png)

*Screenshot of SS8 Working*:

![Android Studio Installation Screenshot](SS8working.png)

*Screenshot of SS9*:

![Android Studio Installation Screenshot](SS9.png)

*Screenshot of SS9*:

![Android Studio Installation Screenshot](SS92.png)









