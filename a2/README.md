> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331

## Derek Piccininni

### Assignment #2 Requirements:

*Sub-Heading:*

1. Screenshot of running application’s unpopulated user interface
2. Screenshot of running application’s populated user interface
3.


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Android Studio - Tip Calculator*:

![Tip Calculator](TipCalc1.png)


*Screenshot of Android Studio - Tip Calculator*:

![Tip Calculator](TipCalc2.png)


*Screenshot Skillset 1 - Circle*:

![SS1](SkillSet1.png)

*Screenshot of Skillset 2 - Multiple Number*:

![SS2](corndog.png)

*Screenshot of Skillset 3 - Nested Structures*:

![SS3](SkillSet3.png)








