

# LIS 4331 

## Derek Piccininni

### LIS 4331 Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App and Contacts App
    - Provide screenshots off installations
    - Create BitBucket repo
    - Complete BitBucket tutorials
    - Provide git command descriptions
	
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Android App
    - Provide screenshot of completed app

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Currency Conversion App
    - Provide Screenshots of Running App and Splash Screen

3. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Interest Calculator App
    - Use SharedPreferences
    - Provide Screenshots of Running App and Splash Screen

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create Audio App
    - Provide Screenshots of Running App and Splash Screen
    - Includes SS7, SS8, SS9

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create RSS feed
    - Provide Screenshots of Running App a
    - Includes SS13, SS14, SS15

6. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create MyUsers App
    - Provide Screenshots of Running App a
   

	



