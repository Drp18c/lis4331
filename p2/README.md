> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331

## Derek Piccininni

### Assignment #P2 Requirements:

*Sub-Heading:*

1. Include splash screen
2. Persistent data: SQLite Database
3. Background Colors or theme
4. Launcher icon or image

#### README.md file should include the following items:

* Screenshots of running my users app


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of MyUsers Splash Screen running*:

![AMPPS Installation Screenshot](p2splash.png)

*Screenshot of MyUsers running*:

![AMPPS Installation Screenshot](p2home.png)

*Screenshot of Adding User*:

![JDK Installation Screenshot](p2add.png)

*Screenshot of Update User*:

![Android Studio Installation Screenshot](p2update2.png)

*Screenshot of Delete User*:

![Android Studio Installation Screenshot](p2delete.png)


