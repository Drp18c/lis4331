> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331

## Derek Piccininni

### Assignment #4 Requirements:

*Sub-Heading:*

1. Create Interest Calculator App
2. Include Splash Screen
3. Must use persistent data

#### README.md file should include the following items:

* Screenshot of running application's splash screen
* screenshot of running application's invalid screen
* Screenshot of running application's valid screen


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Splash Screen*:

![AMPPS Installation Screenshot](a4Splash.png)

*Screenshot of running a4*:

![JDK Installation Screenshot](a4empty2.png)

*Screenshot of a4 with data*:

![Android Studio Installation Screenshot](a4filled.png)

*Screenshot of a4 submitted*:

![Android Studio Installation Screenshot](a4final.png)

*Screenshot of SS10 *:

![Android Studio Installation Screenshot](SS10.png)

*Screenshot of SS11 *:

![Android Studio Installation Screenshot](SS11.png)

*Screenshot of SS12 *:

![Android Studio Installation Screenshot](SS12.png)




